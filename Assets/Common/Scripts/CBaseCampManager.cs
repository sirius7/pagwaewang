﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBaseCampManager : MonoBehaviour {

    public CPlayerStatus _cps;
    public string baseCampAreaName;
    public GameObject[] _cameraSet; // 0 main cam , 1 follow cam

    Transform _playerPos;
   
    private Vector2 _baseCampPos = new Vector2(0,0); // 베이스캠프 내부 위치  

    // 카메라 바꾸기 만들어야함

    private void Start()
    {
        baseCampAreaName = this.gameObject.GetComponent<BoxCollider2D>().name;

        _playerPos = GameObject.Find("PlayerBase").GetComponent<Transform>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            if (baseCampAreaName.Equals("BaseCamp"))
            {
                //Debug.Log("BaseCamp!!!!");
                
                CPlayerStatus._prePlayerPos = _playerPos.position;
                // Debug.Log(CPlayerStatus._prePlayerPos);

                BaseCamp_Inputmovement_cameraSetting();

                _playerPos.position = _baseCampPos;

               
                _cps.whereisPlayer = "basecamp";
                _cps.SaveValue();
            }
            else
            {
                if (baseCampAreaName.Equals("IglooDoor"))
                {
                    //Debug.Log("Igllor!!!!");
                    
                    _playerPos.position = CPlayerStatus._prePlayerPos + new Vector2(-5,0);
                    // Debug.Log(_playerPos.position);

                    World_Inputmovement_cameraSetting();

                    _cps.whereisPlayer = "world";
                    _cps.SaveValue();
                }
            }
        }
    }

    public void BaseCamp_Inputmovement_cameraSetting()
    {
        _cameraSet[0].SetActive(true);
        _cameraSet[1].SetActive(false);
    }
    public void World_Inputmovement_cameraSetting()
    {
        _cameraSet[1].SetActive(true);
        _cameraSet[0].SetActive(false);
    }
}
