﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUpgradeArea : MonoBehaviour {

    public GameObject _upgradePanel;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("Player"))
        {
            _upgradePanel.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            _upgradePanel.SetActive(false);
        }
    }
    
}
