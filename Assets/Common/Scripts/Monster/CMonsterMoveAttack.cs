﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterMoveAttack : CMonsterAttack {
    // 공격 범위
    public float _attackRadius;

    // 몬스터 타겟 체크 컴포넌트 참조
    protected CMonsterTargetChecker _targetChecker;
    // 몬스터 이동 컴포넌트 참조
    protected CMonsterMovement _monsterMove;

    protected override void Awake()
    {
        base.Awake();

        _targetChecker = GetComponent<CMonsterTargetChecker>();
        _monsterMove = GetComponent<CMonsterMovement>();
    }

    protected override void Start()
    {
        base.Start();
        //  몬스터 공격 타겟 체크
        //_targetChecker.FrontTargetCheker(this);
        _targetChecker.CircleAreaTargetCheker(this);
    }

    // 추적 시작
    public void Trace()
    {

    }

    //  공격 준비(시작)
    public override void AttackReady()
    {
        //  이동 정지
        _monsterMove.AttackStop();
        //  공격 준비
        base.AttackReady();
    }

    public override void Attack()
    {
        // 공격
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_attackPoint.position,
            _attackRadius, _monsterState._targetMask);

        Debug.Log("몬스터 공격");

        foreach (Collider2D collider in colliders)
        {
            if (collider.tag == "Player")
            {
                // SendMessageOptions.DontRequireReceiver 예외 무시하고 넘어감
                collider.SendMessage("Damage", SendMessageOptions.DontRequireReceiver);
                return;
            }
        }
    }

    // 공격 종료
    public override void AttackFinish()
    {
        base.AttackFinish();
        //  몬스터 이동 재개
        _monsterMove.MoveResume();
        //  몬스터 공격 타겟 체크
        //_targetChecker.FrontTargetCheker(this);
        _targetChecker.CircleAreaTargetCheker(this);
    }

    public void AttackAnimationEvent()
    {
        Attack();
    }

    public void AttackFinishAnimationEvent()
    {
        AttackFinish();
    }
}
