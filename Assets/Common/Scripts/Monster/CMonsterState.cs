﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterState : CCharacterState {

    // 몬스터 레벨
    [HideInInspector]
    public int _level;

    // 몬스터가 주는 하트
    [HideInInspector]
    public float _heartCount;

    public string _genAreaName;

    private void Awake()
    {
        _level = 0;
        _heartCount = 3;
    }
}
