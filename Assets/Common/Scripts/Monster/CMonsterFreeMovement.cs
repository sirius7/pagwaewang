﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterFreeMovement : CMonsterMovement {

    public Transform _monsterTrans;

    private Vector2 _moveDirection; //  몬스터 이동 벡터

    protected override void Awake()
    {
        base.Awake();
       
    }

    protected override void Start()
    {
        // 기본동작은 플레이어 참조 작업
        base.Start();

        // 일정한 간격으로 랜덤한 목적지 설정
        StartCoroutine("MonsterRandomDirectionCoroutine");
    }

    protected override void Update()
    {
        // 기본동작은 이 캐릭터 상태를 [Move]로 만들고 Move 애니메이션을 반복
        base.Update();
    }

    //  이동 메소드 재정의
    public override void Move()
    {
        base.Move();    //  기본 이동 로직 수행 - Move 애니메이션 반복

        // 추적상태
        if (_characterState.state == CCharacterState.State.Trace)
        {
            // 추적목표위치를 갱신하여 플레이어를 추적
            SetMoveDirection(_player.transform.position - transform.position);
        }

        // 가로 이동에 대해서만 플립체크
        if ((_characterState._isRightDir && transform.position.x <= 0) ||
            (!_characterState._isRightDir && transform.position.x > 0))
        {
            Flip();
        }

        _monsterTrans.Translate(_moveDirection.normalized * _moveSpeed
            * Time.deltaTime);

        _monsterTrans.position = new Vector2(Mathf.Clamp(_monsterTrans.position.x, -77f, -20f), Mathf.Clamp(_monsterTrans.position.y, -28f, 28f));
    }

    // Trigger 충돌 이벤트
    private void OnTriggerEnter2D(Collider2D collision)
    {

    }

    // 몬스터 이동 목적지 설정
    public void SetMoveDirection(Vector2 moveDirection)
    {
        this._moveDirection = moveDirection;
    }

    // 주기마다 랜덤한 몬스터 이동
    IEnumerator MonsterRandomDirectionCoroutine()
    {
        while (true)
        {
            //Debug.Log("목적지 랜덤 설정 상태 : " + _characterState.state);

            // 이동 상태일 경우 목적지는 플레이어가 아닌 랜덤이 됨 
            if (_characterState.state == CCharacterState.State.Move)
            {
                float h = UnityEngine.Random.Range(transform.position.x - 10f, transform.position.x + 10f);
                float v = UnityEngine.Random.Range(transform.position.y - 10f, transform.position.y + 10f);

                Vector2 monsterDestination = new Vector2(h, v);

                //Debug.Log("목적지 랜덤 설정 : " + h + ", " + v);

                SetMoveDirection(monsterDestination - (Vector2)transform.position);
            }

            yield return new WaitForSeconds(5f);
        }
    }
}
