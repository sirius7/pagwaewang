﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterDestroyer : CDestroyer {

    // 파괴 이펙트
    public GameObject _destroyEffectPrefab;

    private CGameManager _gameManager;

    private CPlayerHPManager _playerHpManager;

    private CMonsterState _monsterState;

   
    protected override void Awake()
    {
       
        _monsterState = GetComponent<CMonsterState>();
        this._genAreaName = _monsterState._genAreaName;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        _gameManager = GameObject.Find("GameManager").GetComponent<CGameManager>();
        _playerHpManager = GameObject.Find("PlayerBase").GetComponent<CPlayerHPManager>();

        _detectArea = GameObject.Find(this._genAreaName).GetComponent<CDetectArea>();
    }

    private void Update()
    {
        if(_isAutoDestroy)
        {
            Destroy();
            _isAutoDestroy = false;
        }
    }
    public override void Destroy()
    {
        _detectArea.SendMessage("ReCountRemainMonsters");

        if (_destroyEffectPrefab != null)
        {
            // 오브젝트 파괴 이펙트 생성
            GameObject effect = Instantiate(_destroyEffectPrefab,
                transform.position, Quaternion.identity);
            Destroy(effect, 0.25f);
        }

        // 하트 증가
        _gameManager.HeartUp((int)_monsterState._heartCount);
        _gameManager.UpdateAssetsValue();

        // 플레이어 체력 회복
        _playerHpManager.HpUp(_monsterState._heartCount);

        base.Destroy();
    }
}
