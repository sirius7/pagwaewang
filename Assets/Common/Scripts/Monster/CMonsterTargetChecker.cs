﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterTargetChecker : MonoBehaviour {

    protected CCharacterState _monsterState;    //  몬스터 상태 컴포넌트 참조
    public CCharacterState.State _checkState;   //  체크할 상태 설정

    public float _traceCheckRange;              //  추적 체크 범위
    public float _attackCheckRange;             //  공격 체크 범위
    protected Transform _attackPoint;           //  체크 시작 위치(공격 위치)

    protected Transform _playerPos;             //  플레이어 위치

    private CMonsterFreeMovement _monsterFreeMovement;

    private void Awake()
    {
        _monsterState = GetComponent<CCharacterState>();
        _attackPoint = transform.Find("AttackPoint").transform;
        _monsterFreeMovement = GetComponent<CMonsterFreeMovement>();
    }

    public void CircleAreaTargetCheker(CMonsterAttack monsterAttack)
    {
       // Debug.Log("범위 체크");

        //_playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        _playerPos = GameObject.Find("PlayerBase").transform;

        StartCoroutine("CircleAreaTargetCheckCoroutine", monsterAttack);
    }

    public IEnumerator CircleAreaTargetCheckCoroutine(CMonsterAttack monsterAttack)
    {
        // 몬스터가 내가 초기화에 지정한 상태거나(Move) 추적상태일때 감시를 진행함
        // 현재 몬스터는 계속 이동 상태를 유지하다가 
        // 이 한 프레임 감지거리에 들어왔을때 공격이나 추적상태로 잠깐 변경됨
        // 공격의 경우 이동스피드를 0으로 만들고 
        // 공격 애니메이션을 실행
        // 애니메이션 끝날 때 이벤트로 다시 이동 상태로 변경

        // 추적은 플레이어가 공격범위보다 바깥이지만 추적 범위에 들어왔다면 
        // 추적상태로 변경하고
        // 목표 위치를 플레이어로 변경해 주기만 하면 됨

        // 추적 해제를 위한 타이머
        float timer = 0f;

       // Debug.Log("distance");

        while (_monsterState.state == _checkState ||
            _monsterState.state == CCharacterState.State.Trace)
        {
            float distance = Vector2.Distance(_attackPoint.position, _playerPos.position);

            //  중복되는 반경에선 공격이 우선권을 가짐
            if (distance <= _attackCheckRange)
            {
                // 공격 시작
                monsterAttack.AttackReady();
            }
            else if (distance <= _traceCheckRange)    // 추적거리 내
            {
                timer = 0f;
                _monsterState.state = CCharacterState.State.Trace;
                _monsterFreeMovement.SetMoveDirection(_playerPos.position - transform.position);
            }
            else
            {
                // 반경 바깥땐 이동 스크립트에서 알아서 처리
                // 단 추적 상태에서 반경 밖으로 나간 경우
                // 일정 시간 후 이동 상태로 되돌릴 필요가 있음
                timer += Time.deltaTime;
                if (_monsterState.state == CCharacterState.State.Trace)
                {
                    if (timer >= 5f)    // 추적도중 반경 밖에서 5초가 지나면 추적중단
                    {
                        timer = 0f;
                        StopTrace();
                    }
                }
            }

            yield return null;
        }
    }

    private void StopTrace()
    {
        _monsterState.state = CCharacterState.State.Move;
    }
}
