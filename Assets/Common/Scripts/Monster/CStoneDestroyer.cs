﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CStoneDestroyer : CDestroyer {

    // 파괴 이펙트
    public GameObject _destroyEffectPrefab;

    private CGameManager _gameManager;

    private CStoneState _stoneState;

    protected override void Awake()
    {

        _stoneState = GetComponent<CStoneState>();
        this._genAreaName = _stoneState._genAreaName;
    }
    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        _gameManager = GameObject.Find("GameManager").GetComponent<CGameManager>();
        _detectArea = GameObject.Find(this._genAreaName).GetComponent<CDetectArea>();
    }

    private void Update()
    {
        if (_isAutoDestroy)
        {
            Destroy();
            _isAutoDestroy = false;
        }
    }
    public override void Destroy()
    {
        _detectArea.SendMessage("ReCountRemainRocks");

        if (_destroyEffectPrefab != null)
        {
            // 오브젝트 파괴 이펙트 생성
            GameObject effect = Instantiate(_destroyEffectPrefab,
                transform.position, Quaternion.identity);
            Destroy(effect, 0.25f);
        }

        // 조각 증가
        _gameManager.DustUp(1);
        _gameManager.UpdateAssetsValue();

        base.Destroy();
    }
}
