﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CFireForRecovery : MonoBehaviour {

    public CPlayerHPManager cPlayerHPM;

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("Player"))
        {
            cPlayerHPM.HPRecovery();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            cPlayerHPM.StopHPRecovery();
        }
    }
}
