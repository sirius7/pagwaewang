﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CUpgrade : MonoBehaviour {

    public  CPlayerStatus _cps;
    public Button[] _assetBtn;
    private CGameManager _gm;

    private void OnEnable()
    {
       SetBtnInteractable(_assetBtn);  // AssetCheck ,  업글영역에 들어갔을 때 자원 유무에 따라 자원사용 버튼 활성/비활성화
    }

    protected virtual void Start()
    {
      
        _gm = GameObject.Find("GameManager").GetComponent<CGameManager>();
    }
    void UpdateAssetsValue(int assetType)
    {
        if (assetType == 0)
        {
            _cps.dust -= 100;
            _gm.UpdateAssetsValue();
           
        }
        else
        {
            if (assetType == 1)
            {

                _cps.heart -= 1;
                _gm.UpdateAssetsValue();
               
            }
        }
    }
    public void SetBtnInteractable(Button[] upglBtn)
    {

        if (_cps.dust < 100)
        {
            upglBtn[0].interactable = false;
           
        }
      else
        {
            if (_cps.dust >= 100)
            {
                upglBtn[0].interactable = true;
               
            }
        }

        if (_cps.heart < 1)
        {
            upglBtn[1].interactable = false;
          
        }
        else
        {
            if (_cps.heart >= 1)
            {
                upglBtn[1].interactable = true;
             
            }
        }

    }

    public void SetBtnDisableAll(Button[] upglBtn)
    {
        upglBtn[0].interactable = false;
        upglBtn[1].interactable = false;
    }

        public void ColdResUpgrade(int assetType)
    {
        if (_cps.coldResist >= 1f)
        {
            SetBtnDisableAll(_assetBtn);
            return;
        }
        else
        {
            UpdateAssetsValue(assetType);
            SetBtnInteractable(_assetBtn);
            _cps.coldResist += 0.01f;
        }
    }

    public void SwingSpeedUpgrade(int assetType)
    {
        UpdateAssetsValue(assetType);
        SetBtnInteractable(_assetBtn);

        _cps.swingSpeed += 0.1f;
        _cps._anim.SetFloat("SwingSpeed", _cps.swingSpeed);
    }
    public void PowerUpgrade(int assetType)
    {
        UpdateAssetsValue(assetType);
        SetBtnInteractable(_assetBtn);
        _cps.power += 0.1f;
    }
}
