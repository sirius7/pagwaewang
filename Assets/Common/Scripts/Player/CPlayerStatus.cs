﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CPlayerStatus : MonoBehaviour {

    public float hp ;
    public int dust;
    public int heart;
    public string whereisPlayer;

    public float swingSpeed;
    public float coldResist;
    public float power;

    public static Vector2 _prePlayerPos;  // 메인 맵에서 베이스캠프로 넘어올 시점의 플레이어 위치
    public Vector2 playerPosVcr;

    [HideInInspector]
    public Animator _anim;

    Transform _player;
    CBaseCampManager _cbcm;
    

    private void Awake()
    {
        LoadValue();
        _player = GetComponent<Transform>();
        _anim = GetComponent<Animator>();
        
    }

    private void Start()
    {
        _anim.SetFloat("SwingSpeed", swingSpeed);

        _cbcm = FindObjectOfType<CBaseCampManager>();

        if (whereisPlayer.Equals("world"))
        {
            _cbcm.World_Inputmovement_cameraSetting();

        }
        else
        {
            if (whereisPlayer.Equals("basecamp"))
            {
                _cbcm.BaseCamp_Inputmovement_cameraSetting();
            }
        }
        _player.position = new Vector2(PlayerPrefs.GetFloat("PlayerPosX", 0),PlayerPrefs.GetFloat("PlayerPosY", 0));
    }

    public void SaveValue()
    {
         PlayerPrefs.SetFloat("HP", hp);
         PlayerPrefs.SetInt("DUST", dust);
         PlayerPrefs.SetInt("HEART", heart);
        PlayerPrefs.SetInt("HEART", heart);

        PlayerPrefs.SetString("whereisPlayer",whereisPlayer);
        PlayerPrefs.SetFloat("prePlayerPos.x",_prePlayerPos.x);
        PlayerPrefs.SetFloat("prePlayerPos.y", _prePlayerPos.y);

        PlayerPrefs.SetFloat("SWINGSPEED", swingSpeed);
         PlayerPrefs.SetFloat("COLDRESIST", coldResist);
         PlayerPrefs.SetFloat("POWER", power);

        playerPosVcr = _player.position;
        PlayerPrefs.SetFloat("PlayerPosX", playerPosVcr.x);
        PlayerPrefs.SetFloat("PlayerPosY", playerPosVcr.y);
        PlayerPrefs.Save();
    }
    public void LoadValue()
    {

        hp = PlayerPrefs.GetFloat("HP", 50);
        dust = PlayerPrefs.GetInt("DUST", 1000);
        heart = PlayerPrefs.GetInt("HEART", 3);

        whereisPlayer = PlayerPrefs.GetString("whereisPlayer", "world");
        _prePlayerPos.x = PlayerPrefs.GetFloat("prePlayerPos.x", -50);
        _prePlayerPos.y = PlayerPrefs.GetFloat("prePlayerPos.y", 0);

        swingSpeed = PlayerPrefs.GetFloat("SWINGSPEED", 1);
       // PlayerPrefs.DeleteKey("COLDRESIST");

        coldResist = PlayerPrefs.GetFloat("COLDRESIST", 0);
        power = PlayerPrefs.GetFloat("POWER", 1);

        PlayerPrefs.GetFloat("PlayerPosX",-50f);
        PlayerPrefs.GetFloat("PlayerPosY",0);
    }

    // 테스트용 저장 - 위치 초기화

    public void SaveValueClear()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }


}
