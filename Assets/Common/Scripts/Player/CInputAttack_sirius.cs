﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputAttack_sirius : MonoBehaviour {

    public Transform _attackPoint;
    Animator _anim;

    private CPlayerStatus _cps;
    private int _attacklayermask;
    private string[] _maskName = {"Monster","Rock" };

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _cps = GetComponent<CPlayerStatus>();
        _attacklayermask = LayerMask.GetMask(_maskName);
    }
    
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if(!_anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                _anim.SetTrigger("Attack");
        }
	}

    public void AttackAnimationEvent()
    {
        Attack();
        AttackToStone();
    }

    public void Attack()
    {
        Collider2D hitCol = Physics2D.OverlapCircle(_attackPoint.position, 0.6f, _attacklayermask);

        if(hitCol != null)
        {
            hitCol.SendMessage("Damage", _cps.power, SendMessageOptions.DontRequireReceiver);

        }
    }

    public void AttackToStone()
    {
        Collider2D hitCol = Physics2D.OverlapCircle(_attackPoint.position, 0.3f, 1 << LayerMask.NameToLayer("Stone"));

        if (hitCol != null)
        {
            hitCol.SendMessage("Damage", _cps.power, SendMessageOptions.DontRequireReceiver);

        }
    }
}
