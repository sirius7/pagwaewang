﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPlayerHPManager : MonoBehaviour {

    public float hp;
    public Image hpProgress;
    public GameObject hpRecoveryEffect;

    private CPlayerStatus _cps;

    private void Awake()
    {
        _cps = GetComponent<CPlayerStatus>();
    }
    // Use this for initialization
    void Start () {

        hp = _cps.hp;
        hpProgress.fillAmount = hp * 0.01f;
    }

    private void Update()
    {
       
        hp = _cps.hp;
        hpProgress.fillAmount = hp * 0.01f;
       
        // test
        if(Input.GetKeyUp(KeyCode.Z))
        {
            _cps.dust += 500;
            _cps.heart += 10;
            Debug.Log("Asset UP~");
        }

        // test
        // 플레이어의 저장정보를 모두 날립니다
        //if (Input.GetKeyUp(KeyCode.A))
        //{
        //    _cps.SaveValueClear();
        //}
    }

    IEnumerator Cor_HP_Recovery()
    {
        while(hp < 100)
        {
            _cps.hp += 2;
            hp = _cps.hp;
            hpProgress.fillAmount = hp * 0.01f;
            

            yield return new WaitForSeconds(1f);
        }
        StopHPRecovery();
    }

    public void HPRecovery()
    {
        if (_cps.hp >= 100) return;

        StartCoroutine("Cor_HP_Recovery");
        if(!hpRecoveryEffect.activeSelf)
        {
            hpRecoveryEffect.SetActive(true);
        }
    }
    public void StopHPRecovery()
    {
        StopCoroutine("Cor_HP_Recovery");
        if (hpRecoveryEffect.activeSelf)
        {
            hpRecoveryEffect.SetActive(false);
        }
        Debug.Log("코루틴 종료");
    }

    //----------------- 하트 획득에 의한 hp 회복

    public void HpUp(float value)
    {
        Debug.Log("체력 회복 전" + hpProgress.fillAmount.ToString() + ", " + value);

        _cps.hp += value;
        hp = _cps.hp;
        hpProgress.fillAmount = hp * 0.01f;

        Debug.Log("체력 회복 후" + hpProgress.fillAmount.ToString() + ", " + value);
    }
}
