﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputMovement_sirius : MonoBehaviour {


    public float _speed;

    private Transform player;

    float h, v;
    Vector2 direction;

    //------ 추가
    Animator _animator; //  애니메이터

    public bool _isRightFace;   // 시선 방향

    private CPlayerStatus _cps; //  플레이어 상태

    private void Awake()
    {
        player = this.gameObject.GetComponent<Transform>();

        _animator = GetComponent<Animator>();

        _cps = GetComponent<CPlayerStatus>();
    }
    // Update is called once per frame
    void Update () {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");

        // 가로 이동에 대해서
        if ((_isRightFace && h < 0) ||
            (!_isRightFace && h > 0))
        {
            Flip(); // CMove.Flip() 호출
        }

        direction = new Vector2(h, v);

        if (direction == Vector2.zero)
        {
            _animator.SetBool("Walk", false);
        }
        else  // 키 입력이 있다면
        {
            // 이동 애니메이션 재생
            _animator.SetBool("Walk", true);

        }
    }

    private void FixedUpdate()
    {
        player.Translate(direction * _speed * Time.fixedDeltaTime);

        // 경계 분기
        if(_cps.whereisPlayer.Equals("basecamp"))
        {
            player.position = new Vector2(Mathf.Clamp(player.position.x, -7f, 7f), Mathf.Clamp(player.position.y, -4f, 3f));
        }
        else
        {
            player.position = new Vector2(Mathf.Clamp(player.position.x, -77f, -20f), Mathf.Clamp(player.position.y, -28f, 28f));
        }
        
    }

    // 캐릭터 플립
    public void Flip()
    {
        // Transform의 스케일 벡터 받음
        Vector3 scale = transform.localScale;
        // 시선 반전
        scale.x = scale.x * (-1); // scale.x *= -1;
        transform.localScale = scale;

        // 시선값 반전
        _isRightFace = !_isRightFace;
    }
}
