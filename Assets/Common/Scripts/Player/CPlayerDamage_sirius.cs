﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPlayerDamage_sirius : MonoBehaviour {

    CPlayerStatus _cps;
    Animator _animator;

    private void Awake()
    {
        _cps = GetComponent<CPlayerStatus>();
        _animator = GetComponent<Animator>();

    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if(_cps.whereisPlayer.Equals("world"))
        {
            if (_cps.hp <= 0)
                return;

            DamagedFromBlizzard(_cps.coldResist);
        }
    }

    public void DamagedFromBlizzard(float coldres)
    {
        _cps.hp -= ((1 - coldres) * Time.deltaTime);
    }

    public void Damage()
    {
        Debug.Log("플레이어 데미지");

        // 피격 애니메이션을 실행함
        _animator.Play("Damage", 1);

        // 데미지 처리가 필요함
    }
}
