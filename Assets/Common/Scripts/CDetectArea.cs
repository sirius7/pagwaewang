﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDetectArea : MonoBehaviour {

    public int _monsterRemainCount;  // 남은 몬스터숫자
    public int _RockRemainCount;

    public string _myAreaName;
    private BoxCollider2D _col;
   
    CGenerator _generator;

    private void Awake()
    {
        _monsterRemainCount = 0;
        _myAreaName = this.gameObject.transform.name;
        _col = this.gameObject.GetComponent<BoxCollider2D>();
    }
    // Use this for initialization
    void Start() {
        _generator = GameObject.Find("Generator").GetComponent<CGenerator>();

    }
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            if (_monsterRemainCount <= 1)
            {
                _generator.GenerateMonster(_col, _myAreaName ,out _monsterRemainCount);

            }
            else
            {
                return;
            }

            if(_RockRemainCount <=1)
            {
                _generator.GenerateRocks(_col, _myAreaName, out _RockRemainCount);
            }
            else
            {
                return;
            }
        }
    }

   public void ReCountRemainMonsters()
    {
      
            Debug.Log("---");
            if (_monsterRemainCount > 0)
            {
                _monsterRemainCount--;
                
            }
            else
            {
                if (_monsterRemainCount <= 0)
                {
                    _monsterRemainCount = 0;
                  
                }
            }
            
    }
    public void ReCountRemainRocks()
    {

        Debug.Log("---");
        if (_RockRemainCount > 0)
        {
            _RockRemainCount--;

        }
        else
        {
            if (_RockRemainCount <= 0)
            {
                _RockRemainCount = 0;
            }
        }

    }
}
