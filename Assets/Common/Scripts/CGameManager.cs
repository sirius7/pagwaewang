﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CGameManager : MonoBehaviour {

   
    public Text[] _assetsText;

    CPlayerStatus _cps;

    // Use this for initialization
    void Start () {
        _cps = FindObjectOfType<CPlayerStatus>();

        _assetsText[0].text = _cps.dust.ToString();
        _assetsText[1].text = _cps.heart.ToString();
    }

    public void UpdateAssetsValue()
    {
        _assetsText[0].text = _cps.dust.ToString();
        _assetsText[1].text = _cps.heart.ToString();
    }

    public void HeartUp(int i)
    {
        _cps.heart += i;
    }
    public void DustUp(int i)
    {
        _cps.dust += i;
    }

 

    
}
